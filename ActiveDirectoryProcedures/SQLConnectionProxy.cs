using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
//using System.Linq;
using System.Text;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Base proxy class for connecting to SQL server and executing stored procedure
	/// </summary>
	[Serializable]
	public abstract class SQLConnectionProxy : IDisposable
	{
		/// <summary>
		/// String for establishing connection in SQL server context
		/// </summary>
		private static readonly string contextConnection = "context connection=true";
		private readonly SqlConnection connection;

		protected SQLConnectionProxy()
		{
			connection = new SqlConnection(contextConnection);
			connection.Open();
		}
		
		#region Dispose pattern

		private bool disposed = false;
		
		public void Dispose()
		{
			// Dispose of unmanaged resources.
			Dispose(true);
			// Suppress finalization.
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposed)
				return;

			if (disposing)
			{
				if (connection != null && connection.State == ConnectionState.Open) 
					connection.Close();
			}

			// Free any unmanaged objects here. 
			//
			disposed = true;
		}

		// for unmanaged resource disposing

		~SQLConnectionProxy()
		{
			Dispose(false);
		}

		#endregion

		/// <summary>
		/// Execute stored procedure name <paramref name="sqlProcedureName"/> with optional <paramref name="autoCommit"/> upon execution 
		/// </summary>
		public void ExecuteStoredProcedure(string sqlProcedureName, bool autoCommit = false)
		{
			if (string.IsNullOrEmpty(sqlProcedureName)) throw new ArgumentNullException("sqlProcedureName");
			if (connection == null || connection.State != ConnectionState.Open) throw new InvalidOperationException("Connection is not opened!");

			SqlTransaction transaction =  null;
			try
			{
				SQLCommand = CreateSQLCommand(connection, sqlProcedureName);
				if (autoCommit)
				{
					transaction = connection.BeginTransaction();
					SQLCommand.Transaction = transaction;
				}

				SQLCommand.Parameters.AddRange(SQLParameters.ToArray());
				SQLCommand.ExecuteNonQuery();
				if (autoCommit)
					transaction.Commit();
			}
			catch (Exception ex)
			{
				if (autoCommit && transaction != null)
					transaction.Rollback();

				throw new InvalidOperationException(CreateMessage(SQLCommand, ex), ex.InnerException);
			}
		}

		private string CreateMessage(SqlCommand command, Exception exception)
		{
			var sb = new StringBuilder(exception.Message + exception.StackTrace);
			sb.Append(command.CommandText);
			sb.Append("Parameters: ");
			foreach (SqlParameter parameter in command.Parameters)
			{
				sb.AppendFormat("{0}:{1} \n", parameter.ParameterName, parameter.Value);
			}
			
			return sb.ToString();
		}

		/// <summary>
		/// List of SQL parameters supplied for SP. Must be specified by overriden class
		/// </summary>
		protected IEnumerable<SqlParameter> SQLParameters { get; set; }

		/// <summary>
		/// SQL command used for executing SP
		/// </summary>
		protected SqlCommand SQLCommand { get; private set; }

		/// <summary>
		/// Convert generic null to DBNull value and according to DB
		/// </summary>
		protected static object ConvertToDb(object value, SqlDbType dataType = SqlDbType.NVarChar)
		{
			if (value != null && dataType == SqlDbType.DateTime)
			{
				long res;
				if (Int64.TryParse(value.ToString(), out res))
				{
					// this hack required to deal with the fact that SqlDateTime start with 1753 and DateTime.FromFileTimeUtc from 1601. 
					// see more http://stackoverflow.com/questions/3310569/what-is-the-significance-of-1-1-1753-in-sql-server
					// so in case of 0 - we just returning null to indicate no value exists
					if (res == 0)
						return DBNull.Value;
					return DateTime.FromFileTimeUtc(res);
				}
			}
			if (value != null && dataType == SqlDbType.UniqueIdentifier)
			{
				var bytes = value as byte[];
				if (bytes != null)
					return new Guid(bytes);
			}

			return value ?? DBNull.Value;
		}

		private static SqlCommand CreateSQLCommand(SqlConnection connection, string procedureName)
		{
			return new SqlCommand
				       {
					       Connection = connection,
					       CommandType = CommandType.StoredProcedure,
					       CommandText = procedureName
				       };
		}
	}
}