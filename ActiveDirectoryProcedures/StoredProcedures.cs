﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;
using Microsoft.SqlServer.Server;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Class for main stored procedures and user-function logic
	/// </summary>
	public class StoredProcedures
	{
		/*
		/// <summary>
		/// SQL CLR stored procedure that reads user and usergroup data from AD and insert it into appropriate table
		/// </summary>
		/// <param name="ldapPath">Acive Directory LDAP path</param>
		/// <param name="domainUserId">Active Directory user name</param>
		/// <param name="domainPassword">Active Directory user password</param>
		/// <param name="isBatch">Is process a batch or in single request</param>
		/// <param name="entityRecID">Identifier of the organization</param>
		/// <param name="processDescription">Optional process description</param>
		/// <param name="canonicalName">Optional canonical name for the single user</param>
		[SqlProcedure]
		public static void ReadADUsersAndUserGroups(string ldapPath, string domainUserId, string domainPassword, bool isBatch, int entityRecID, string processDescription, string canonicalName = null)
		{
			if (string.IsNullOrEmpty(ldapPath)) throw new ArgumentNullException("ldapPath");
			if (entityRecID < 0) throw new ArgumentNullException("entityRecID");

			int processRecId = ProcessReadADStart(entityRecID, processDescription);
			var searcher = new ActiveDirectorySearcher();
			string errors = null;
			try
			{
				var properties = searcher.SearchAD(ldapPath, domainUserId, domainPassword, canonicalName);
				foreach (ADProperties property in properties)
				{
					try
					{
						WriteADUserIntoSQL(property, processRecId, isBatch);
						foreach (var pair in searcher.SplitDistinguishedNameIntoDict(property))
						{
							WriteADUSerContextBatchIntoSQL(property, pair, processRecId, isBatch);
						}
					}
					catch (Exception e)
					{
						errors += GetExceptionErrors(e);
					}
				}
			}
			catch (Exception e)
			{
				errors += GetExceptionErrors(e);
			}
			finally
			{
				ProcessReadADEnd(processRecId, errors);	
			}
		}
		*/
		private static string GetExceptionErrors(Exception e)
		{
			var s = e.Message + e.StackTrace;
			if (e.InnerException != null)
			{
				s += "Inner: " + e.Message + e.StackTrace;
			}
			return s + " ";
		}

		/// <summary>
		/// SQL CLR stored procedure for reading web service and inserting resulted XML in a table
		/// </summary>
		/// <param name="entityRecId">Identifier of the organization</param>
		/// <param name="externalSourceRecID"></param>
		/// <param name="externalSourceKey"></param>
		/// <param name="sourceURLCalled">Source URL of the webservice</param>
		[SqlProcedure]
		public static void ReadXMLWebService(int entityRecId, int externalSourceRecID, string externalSourceKey, string sourceURLCalled)
		{
			var webServiceReceiverRecID = ProcessWebServiceStart(entityRecId, externalSourceRecID, externalSourceKey, sourceURLCalled);

			string errorMessages = null;
			string sourceXMLData = null;
			try
			{
				sourceXMLData = WebServiceReader.Read(sourceURLCalled);
			}
			catch (Exception e)
			{
				errorMessages += GetExceptionErrors(e);
			}
			finally
			{
				ProcessWebServiceEnd(webServiceReceiverRecID, sourceXMLData, errorMessages);
			}
		}

		/// <summary>
		/// SQL CLR User-Function for acquiring web page using GET request
		/// </summary>
		/// <param name="URL">URL to get web page</param>
		/// <param name="UserID">Username for accesing webpage (optional)</param>
		/// <param name="Password">Password for accesing webpage (optional)</param>
		/// <param name="Domain">Domain for authenticatio (optional)</param>
		/// <returns>Webpage content</returns>
		[SqlFunction(DataAccess = DataAccessKind.Read)]
		public static SqlString CaSyncGetWebPage(SqlString URL, SqlString UserID, SqlString Password, SqlString Domain)
		{
			if (URL == null) throw new ArgumentNullException("URL");

			var helper = new WebRequestHelper();
			helper.CreateWebRequest(URL, UserID, Password, Domain);
			return helper.ReadResponseToSQLString();
		}

		/// <summary>
		/// SQL CLR User-Function for acquiring web page using POST request
		/// </summary>
		/// <param name="URL">URL to get web page</param>
		/// <param name="postData">POST data</param>
		/// <param name="UserID">Username for accesing webpage (optional)</param>
		/// <param name="Password">Password for accesing webpage (optional)</param>
		/// <param name="Domain">Domain for authenticatio (optional)</param>
		/// <returns>Webpage content</returns>
		[SqlFunction(DataAccess = DataAccessKind.Read)]
		public static SqlString CaSyncPostWebPage(SqlString URL, SqlString postData, SqlString UserID, SqlString Password, SqlString Domain)
		{
			if (URL == null) throw new ArgumentNullException("uri");

			var helper = new WebRequestHelper();
			helper.CreateWebRequest(URL, UserID, Password, Domain);
			helper.AddPostSpecs();
			helper.PostDataToStream(postData);
			return helper.ReadResponseToSQLString();
		}

		#region Private methods

		private static void ProcessWebServiceEnd(int webServiceReceiverRecID, string sourceXMLData, string errorMessages)
		{
			using (var connection = new SQLWebserviceEnd(webServiceReceiverRecID, sourceXMLData, errorMessages))
			{
				connection.ExecuteStoredProcedure("spu_WebServiceReceiverEnd", true);
			}
		}

		private static int ProcessWebServiceStart(int entityRecId, int externalSourceRecID, string externalSourceKey, string sourceURLCalled)
		{
			int webServiceReceiverRecID = 0;
			using (var connection = new SQLWebserviceStart(entityRecId, externalSourceRecID, externalSourceKey, sourceURLCalled))
			{
				connection.ExecuteStoredProcedure("spi_WebServiceReceiverStart", true);
				object parameter = connection.OutputParameter;
				if (parameter != null)
				{
					webServiceReceiverRecID = Convert.ToInt32(parameter);
				}
			}
			return webServiceReceiverRecID;
		}
		/*
		private static void WriteADUserIntoSQL(ADProperties properties, int processRecId, bool isBatch)
		{
			using (var connection = new SQLConnectionADUserBatch(ADPropertiesManager.GetAllProperties(properties), processRecId))
			{
				connection.ExecuteStoredProcedure("spi_ActiveDirectoryUserBatch", !isBatch);
			}
		}

		private static void WriteADUSerContextBatchIntoSQL(ADProperties properties, KeyValuePair<string, string> attribValuePair, int processRecID, bool isBatch)
		{
			using (var connection = new SQLConnectionADUserContextBatch(properties, attribValuePair, processRecID))
			{
				connection.ExecuteStoredProcedure("spi_ActiveDirectoryUsersContextBatch", !isBatch);
			}
		}
		
		private static void ProcessReadADEnd(int processRecId, string errors)
		{
			using (var connection = new SQLConnectionProcessEnd(processRecId, errors))
			{
				connection.ExecuteStoredProcedure("spu_ActiveDirectoryProcessHeader_End", true);
			}
		}
		
		private static int ProcessReadADStart(int entityRecID, string processDescription)
		{
			using (var connection = new SQLConnectionProcessStart(entityRecID, processDescription))
			{
				connection.ExecuteStoredProcedure("spi_ActiveDirectoryProcessHeader_Start", true);
				object value = connection.OutputParameter;
				if (value != null)
				{
					return Convert.ToInt32(value);
				}
			}
			return 0;
		}
		*/
		#endregion
	}
	
}