using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Implementation class for executing insert AD into UserBatch table stored procedure
	/// </summary>
	[Serializable]
	public class SQLConnectionADUserBatch : SQLConnectionProxy
	{
		public SQLConnectionADUserBatch(IEnumerable<PropertyItem> propertyItems, int processRecId)
		{
			if (propertyItems == null) throw new ArgumentNullException("propertyItems");

			SQLParameters = CreateInsertUserParameters(propertyItems, processRecId);
		}

		private IEnumerable<SqlParameter> CreateInsertUserParameters(IEnumerable<PropertyItem> propertyItems, int processRecId)
		{
			yield return new SqlParameter("@ProcessRecID", SqlDbType.Int)
				             {
								 Value = processRecId
				             };
			foreach (PropertyItem item in propertyItems)
			{
				yield return new SqlParameter(item.Metadata.Name, item.Metadata.SqlDbType, (int)item.Metadata.MaxLength)
				{
					Value = ConvertToDb(item.Value, item.Metadata.SqlDbType)
				};
			}
		}
	}
}
