﻿CREATE Procedure [dbo].[spi_ActiveDirectoryUsersContextBatch](@ProcessRecID int
,@SamAccountName nvarchar(100)
,@Context nvarchar(50)
,@ContextData nvarchar(500))
AS
BEGIN
DECLARE @ADRecID INT
Select @ADRecID = ADRecID
from dbo.TempActiveDirectoryUser
WHERE [SamAccountName] = @SamAccountName
AND ProcessRecID = @ProcessRecID
INSERT INTO [dbo].[TempActiveDirectoryUsersContext]
([ProcessRecID]
,[ADRecID]
,[SamAccountName]
,[Context]
,[ContextData])
VALUES
(@ProcessRecID
,@ADRecID
,@SamAccountName
,@Context
,@ContextData)
END
RETURN 0
