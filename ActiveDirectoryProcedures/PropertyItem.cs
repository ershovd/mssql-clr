using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.SqlServer.Server;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Represents binding between AD and SQL column names with storing attribute value
	/// </summary>
	[Serializable]
	public class PropertyItem
	{
		private readonly string sqlColumnName;
		private UserAccountControl? accountControlflag;
		private object value;

		public PropertyItem(string sqlColumnName)
		{
			this.sqlColumnName = sqlColumnName;

			var pair = GetLDAPNameForSQLName();
			ADName = pair.Key;
			Metadata = pair.Value;
		}
		/// <summary>
		/// Active Directory (LDAP) attribute name
		/// </summary>
		public string ADName { get; private set; }
		/// <summary>
		/// Metadata for SQL column storing appropriate attribute
		/// </summary>
		public SqlMetaData Metadata { get; private set; }
		/// <summary>
		/// Attribute value
		/// </summary>
		public object Value
		{
			get
			{
				if (accountControlflag != null && value != null)
				{
					return GetValueFromFlag();
				}
				
				return value;
			}
			set { this.value = value; }
		}
		/// <summary>
		/// Get value from inner field using UserAccountControl flags
		/// </summary>
		/// <returns></returns>
		private bool GetValueFromFlag()
		{
			UserAccountControl flagValue = accountControlflag.GetValueOrDefault();

			return ((UserAccountControl) value & flagValue) == flagValue;
		}
		/// <summary>
		/// Create a mapping between LDAP and SQL name; initailiaze Metadata property
		/// </summary>
		private KeyValuePair<string, SqlMetaData> GetLDAPNameForSQLName()
		{
			if ("AccountLockoutTime" == sqlColumnName)
				return CreateTupleDateTime("lockoutTime");
			if ("CannotChangePassword" == sqlColumnName)
				return CreateUserAccountControlFlag(UserAccountControl.PASSWD_CANT_CHANGE);
			if ("CanonicalName" == sqlColumnName)
				return CreateTupleNVarChar1000("CanonicalName");
			if ("City" == sqlColumnName)
				return CreateTupleNVarChar100("l");
			if ("CN" == sqlColumnName)
				return CreateTupleNVarChar100("cn");
			if ("Company" == sqlColumnName)
				return CreateTupleNVarChar100("company");
			if ("Country" == sqlColumnName)
				return CreateTupleNVarChar100("c");
			if ("countryCode" == sqlColumnName)
				return CreateKeyValue("countryCode", SqlDbType.Int);
			if ("Created" == sqlColumnName)
				return CreateTupleDateTime("creationTime");
			if ("createTimeStamp" == sqlColumnName)
				return CreateTupleDateTime("createTimeStamp");
			if ("Deleted" == sqlColumnName)
				return CreateKeyValue("isDeleted", SqlDbType.Bit);
			if ("Department" == sqlColumnName)
				return CreateTupleNVarChar100("department");
			if ("Description" == sqlColumnName)
				return CreateTupleNVarChar1000("description");
			if ("DisplayName" == sqlColumnName)
				return CreateTupleNVarChar1000("displayName");
			if ("DistinguishedName" == sqlColumnName)
				return CreateTupleNVarChar1000("distinguishedName");
			if ("Division" == sqlColumnName)
				return CreateTupleNVarChar100("division");
			if ("EmailAddress" == sqlColumnName)
				return CreateKeyValue("mail", SqlDbType.NVarChar, 200);
			if ("EmployeeID" == sqlColumnName)
				return CreateTupleNVarChar100("employeeID");
			if ("EmployeeNumber" == sqlColumnName)
				return CreateKeyValue("employeeNumber", SqlDbType.Int);
			if ("Enabled" == sqlColumnName)
				return CreateKeyValue("Enabled", SqlDbType.Bit);
			if ("Fax" == sqlColumnName)
				return CreateTupleNVarChar100("facsimileTelephoneNumber");
			if ("GivenName" == sqlColumnName)
				return CreateTupleNVarChar100("givenName");
			if ("HomeDirectory" == sqlColumnName)
				return CreateTupleNVarChar1000("homeDirectory");
			if ("HomedirRequired" == sqlColumnName)
				return CreateUserAccountControlFlag(UserAccountControl.HOMEDIR_REQUIRED);
			if ("HomeDrive" == sqlColumnName)
				return CreateTupleNVarChar100("homeDrive");
			if ("HomePage" == sqlColumnName)
				return CreateKeyValue("wWWHomePage", SqlDbType.NVarChar, 200);
			if ("HomePhone" == sqlColumnName)
				return CreateTupleNVarChar100("homePhone");
			if ("Initials" == sqlColumnName)
				return CreateKeyValue("initials", SqlDbType.NVarChar, 50);
			if ("LastBadPasswordAttempt" == sqlColumnName)
				return CreateTupleDateTime("badPasswordTime");
			if ("LastLogonDate" == sqlColumnName)
				return CreateTupleDateTime("lastLogon");
			if ("LockedOut" == sqlColumnName)
				return CreateUserAccountControlFlag(UserAccountControl.LOCKOUT);
			if ("logonCount" == sqlColumnName)
				return CreateKeyValue("logonCount", SqlDbType.Int);
			if ("Manager" == sqlColumnName)
				return CreateTupleNVarChar100("manager");
			if ("MobilePhone" == sqlColumnName)
				return CreateTupleNVarChar100("mobile");
			if ("Modified" == sqlColumnName)
				return CreateTupleDateTime("whenChanged");
			if ("Name" == sqlColumnName)
				return CreateTupleNVarChar1000("name");
			if ("ObjectClass" == sqlColumnName)
				return CreateTupleNVarChar100("objectClass");
			if ("ObjectGUID" == sqlColumnName)
				return CreateKeyValue("objectGUID", SqlDbType.UniqueIdentifier);
			if ("Office" == sqlColumnName)
				return CreateTupleNVarChar100("physicalDeliveryOfficeName");
			if ("OfficePhone" == sqlColumnName)
				return CreateTupleNVarChar100("otherTelephone");
			if ("Organization" == sqlColumnName)
				return CreateTupleNVarChar100("o");
			if ("OtherName" == sqlColumnName)
				return CreateTupleNVarChar100("middleName");
			if ("PasswordExpired" == sqlColumnName)
				return CreateKeyValue("msDS-UserPasswordExpired", SqlDbType.Bit);
			if ("PasswordLastSet" == sqlColumnName)
				return CreateTupleDateTime("pwdLastSet");
			if ("PasswordNeverExpires" == sqlColumnName)
				return CreateKeyValue("msDS-UserDontExpirePassword", SqlDbType.Bit);
			if ("PasswordNotRequired" == sqlColumnName)
				return CreateKeyValue("ms-DS-UserPasswordNotRequired", SqlDbType.Bit);
			if ("POBox" == sqlColumnName)
				return CreateTupleNVarChar100("postOfficeBox");
			if ("PostalCode" == sqlColumnName)
				return CreateTupleNVarChar100("postalCode");
			if ("sAMAccountName" == sqlColumnName)
				return CreateTupleNVarChar100("sAMAccountName");
			if ("sAMAccountType" == sqlColumnName)
				return CreateKeyValue("sAMAccountType", SqlDbType.Int);
			if ("ScriptPath" == sqlColumnName)
				return CreateTupleNVarChar1000("scriptPath");
			if ("SmartcardLogonRequired" == sqlColumnName)
				return CreateUserAccountControlFlag(UserAccountControl.SMARTCARD_REQUIRED);
			if ("State" == sqlColumnName)
				return CreateTupleNVarChar100("st");
			if ("StreetAddress" == sqlColumnName)
				return CreateTupleNVarChar1000("street");
			if ("Surname" == sqlColumnName)
				return CreateTupleNVarChar100("sn");
			if ("Title" == sqlColumnName)
				return CreateTupleNVarChar100("title");
			if ("userAccountControl" == sqlColumnName)
				return CreateKeyValue("userAccountControl", SqlDbType.Int);
			if ("UserPrincipalName" == sqlColumnName)
				return CreateTupleNVarChar100("userPrincipalName");

			throw new InvalidOperationException();
		}

		private KeyValuePair<string, SqlMetaData> CreateUserAccountControlFlag(UserAccountControl flagValue)
		{
			accountControlflag = flagValue;
			return CreateKeyValue("userAccountControl", SqlDbType.Int);
		}

		private KeyValuePair<string, SqlMetaData> CreateKeyValue(string adName, SqlDbType dataType, int size)
		{
			return new KeyValuePair<string, SqlMetaData>(adName, new SqlMetaData(this.sqlColumnName, dataType, size));
		}

		private KeyValuePair<string, SqlMetaData> CreateKeyValue(string adName, SqlDbType dataType)
		{
			return new KeyValuePair<string, SqlMetaData>(adName, new SqlMetaData(this.sqlColumnName, dataType));
		}

		private KeyValuePair<string, SqlMetaData> CreateTupleNVarChar100(string adName)
		{
			return CreateKeyValue(adName, SqlDbType.NVarChar, 100);
		}

		private KeyValuePair<string, SqlMetaData> CreateTupleNVarChar1000(string adName)
		{
			return CreateKeyValue(adName, SqlDbType.NVarChar, 1000);
		}

		private KeyValuePair<string, SqlMetaData> CreateTupleDateTime(string adName)
		{
			return CreateKeyValue(adName, SqlDbType.DateTime);
		}

		public override string ToString()
		{
			return string.Format("{0}:{1}", Metadata.Name, Metadata.SqlDbType);
		}
	}
}