namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Storage class for Active Directory attributes
	/// </summary>
	public class ADProperties
	{
		public PropertyItem AccountLockoutTime { get; set; }
		public PropertyItem CannotChangePassword { get; set; }
		public PropertyItem CanonicalName { get; set; }
		public PropertyItem City { get; set; }
		public PropertyItem CN { get; set; }
		public PropertyItem Company { get; set; }
		public PropertyItem Country { get; set; }
		public PropertyItem countryCode { get; set; }
		public PropertyItem Created { get; set; }
		public PropertyItem createTimeStamp { get; set; }
		public PropertyItem Deleted { get; set; }
		public PropertyItem Department { get; set; }
		public PropertyItem Description { get; set; }
		public PropertyItem DisplayName { get; set; }
		public PropertyItem DistinguishedName { get; set; }
		public PropertyItem Division { get; set; }
		public PropertyItem EmailAddress { get; set; }
		public PropertyItem EmployeeID { get; set; }
		public PropertyItem EmployeeNumber { get; set; }
		public PropertyItem Enabled { get; set; }
		public PropertyItem Fax { get; set; }
		public PropertyItem GivenName { get; set; }
		public PropertyItem HomeDirectory { get; set; }
		public PropertyItem HomedirRequired { get; set; }
		public PropertyItem HomeDrive { get; set; }
		public PropertyItem HomePage { get; set; }
		public PropertyItem HomePhone { get; set; }
		public PropertyItem Initials { get; set; }
		public PropertyItem LastBadPasswordAttempt { get; set; }
		public PropertyItem LastLogonDate { get; set; }
		public PropertyItem LockedOut { get; set; }
		public PropertyItem logonCount { get; set; }
		public PropertyItem Manager { get; set; }
		public PropertyItem MobilePhone { get; set; }
		public PropertyItem Modified { get; set; }
		public PropertyItem Name { get; set; }
		public PropertyItem ObjectClass { get; set; }
		public PropertyItem ObjectGUID { get; set; }
		public PropertyItem Office { get; set; }
		public PropertyItem OfficePhone { get; set; }
		public PropertyItem Organization { get; set; }
		public PropertyItem OtherName { get; set; }
		public PropertyItem PasswordExpired { get; set; }
		public PropertyItem PasswordLastSet { get; set; }
		public PropertyItem PasswordNeverExpires { get; set; }
		public PropertyItem PasswordNotRequired { get; set; }
		public PropertyItem POBox { get; set; }
		public PropertyItem PostalCode { get; set; }
		public PropertyItem sAMAccountName { get; set; }
		public PropertyItem sAMAccountType { get; set; }
		public PropertyItem ScriptPath { get; set; }
		public PropertyItem SmartcardLogonRequired { get; set; }
		public PropertyItem State { get; set; }
		public PropertyItem StreetAddress { get; set; }
		public PropertyItem Surname { get; set; }
		public PropertyItem Title { get; set; }
		public PropertyItem userAccountControl { get; set; }
		public PropertyItem UserPrincipalName { get; set; }
	}
}