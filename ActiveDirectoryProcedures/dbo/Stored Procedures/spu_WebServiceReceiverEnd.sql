﻿Create Procedure [dbo].[spu_WebServiceReceiverEnd] (@WebServiceReceiverRecID int
,@SourceXMLData nvarchar(max)
,@ErrorMessages nvarchar(max))
AS
BEGIN
UPDATE [dbo].[WebServiceReceiver]
SET [SourceXMLData] = @SourceXMLData
,[DateTimeCompleted] = GetDate()
,[ErrorMessages] = @ErrorMessages
WHERE WebServiceReceiverRecID = @WebServiceReceiverRecID
END
RETURN 0
