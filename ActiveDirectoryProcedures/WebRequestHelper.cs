﻿using System;
using System.Collections;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Helper class for using GET and POST request and getting responses
	/// </summary>
	[Serializable]
	public class WebRequestHelper
	{
		private static readonly string userAgentName = "Syntech SQL CLR Web client";
		private WebRequest webRequest;

		/// <summary>
		/// Add Post data to datastream
		/// </summary>
		public void PostDataToStream(SqlString postData)
		{
			if (postData == null) return;
			byte[] encodeToByteArray = EncodeToByteArray(postData);
			using (Stream dataStream = webRequest.GetRequestStream())
			{
				dataStream.Write(encodeToByteArray, 0, encodeToByteArray.Length);
				dataStream.Close();
			}
		}

		private static byte[] EncodeToByteArray(SqlString postData)
		{
			return Encoding.UTF8.GetBytes(Convert.ToString(postData));
		}

		/// <summary>
		/// Set up the request, including authentication
		/// </summary>
		public void CreateWebRequest(SqlString uri, SqlString username, SqlString passwd, SqlString domain)
		{
			webRequest = WebRequest.Create(Convert.ToString(uri));
			((HttpWebRequest)webRequest).UserAgent = userAgentName;
			if (!username.IsNull)
			{
				if (!domain.IsNull) 
				{
					webRequest.Credentials = new NetworkCredential(Convert.ToString(username), Convert.ToString(passwd), Convert.ToString(domain));
				}
				else
				{
					webRequest.Credentials = new NetworkCredential(Convert.ToString(username), Convert.ToString(passwd));
				}
			}
		}

		/// <summary>
		/// Add POST paramenters
		/// </summary>
		public void AddPostSpecs()
		{
			webRequest.Method = "POST";
			webRequest.ContentType = "application/x-www-form-urlencoded";
		}

		/// <summary>
		/// Collect the response, put it in the string variable "document"
		/// </summary>
		public SqlString ReadResponseToSQLString()
		{
			try
			{
				using (var resp = webRequest.GetResponse())
				{
					Stream stream = resp.GetResponseStream();
					if (stream != null)
					{
						using (var responseStream = stream)
						{
							using (StreamReader rdr = new StreamReader(responseStream))
							{
								var data = rdr.ReadToEnd();
								return HtmlDecode(data);
							}
						}
					}
					return null;
				}
			}
			catch (Exception ex)
			{
				return "Error: " +  ex.Message;
			}
		}

		
		#region Dirty hack - copy of HtmlDecode to avoid adding Web reference
		/// <summary>
		/// Converts a string that has been HTML-encoded for HTTP transmission into a decoded string.
		/// 
		/// </summary>
		/// 
		/// <returns>
		/// A decoded string.
		/// 
		/// </returns>
		/// <param name="s">The string to decode.
		///                 </param>
		private static string HtmlDecode(string s)
		{
			if (s == null)
				return (string)null;
			if (s.IndexOf('&') < 0)
				return s;
			StringBuilder sb = new StringBuilder();
			StringWriter stringWriter = new StringWriter(sb);
			HtmlDecode(s, (TextWriter)stringWriter);
			return ((object)sb).ToString();
		}

		private static char[] s_entityEndingChars = new char[2]
		{
		  ';',
		  '&'
		};

		/// <summary>
		/// Converts a string that has been HTML-encoded into a decoded string, and sends the decoded string to a <see cref="T:System.IO.TextWriter"/> output stream.
		/// 
		/// </summary>
		/// <param name="s">The string to decode.
		///                 </param><param name="output">A <see cref="T:System.IO.TextWriter"/> stream of output.
		///                 </param>
		private static void HtmlDecode(string s, TextWriter output)
		{
			if (s == null)
				return;
			if (s.IndexOf('&') < 0)
			{
				output.Write(s);
			}
			else
			{
				int length = s.Length;
				for (int index1 = 0; index1 < length; ++index1)
				{
					char ch1 = s[index1];
					if ((int)ch1 == 38)
					{
						int index2 = s.IndexOfAny(s_entityEndingChars, index1 + 1);
						if (index2 > 0 && (int)s[index2] == 59)
						{
							string entity = s.Substring(index1 + 1, index2 - index1 - 1);
							if (entity.Length > 1)
							{
								if ((int)entity[0] == 35)
								{
									try
									{
										ch1 = (int)entity[1] == 120 || (int)entity[1] == 88 ? (char)int.Parse(entity.Substring(2), NumberStyles.AllowHexSpecifier) : (char)int.Parse(entity.Substring(1));
										index1 = index2;
										goto label_15;
									}
									catch (FormatException ex)
									{
										++index1;
										goto label_15;
									}
									catch (ArgumentException ex)
									{
										++index1;
										goto label_15;
									}
								}
							}
							index1 = index2;
							char ch2 = HtmlEntities.Lookup(entity);
							if ((int)ch2 != 0)
							{
								ch1 = ch2;
							}
							else
							{
								output.Write('&');
								output.Write(entity);
								output.Write(';');
								continue;
							}
						}
					}
				label_15:
					output.Write(ch1);
				}
			}
		}
		internal class HtmlEntities
		{
			private static object _lookupLockObject = new object();
			private static string[] _entitiesList = new string[252]
    {
      "\"-quot",
      "&-amp",
      "<-lt",
      ">-gt",
      " -nbsp",
      "¡-iexcl",
      "¢-cent",
      "£-pound",
      "¤-curren",
      "¥-yen",
      "¦-brvbar",
      "§-sect",
      "¨-uml",
      "©-copy",
      "ª-ordf",
      "«-laquo",
      "¬-not",
      "\x00AD-shy",
      "®-reg",
      "¯-macr",
      "°-deg",
      "±-plusmn",
      "\x00B2-sup2",
      "\x00B3-sup3",
      "´-acute",
      "µ-micro",
      "¶-para",
      "·-middot",
      "¸-cedil",
      "\x00B9-sup1",
      "º-ordm",
      "»-raquo",
      "\x00BC-frac14",
      "\x00BD-frac12",
      "\x00BE-frac34",
      "¿-iquest",
      "À-Agrave",
      "Á-Aacute",
      "Â-Acirc",
      "Ã-Atilde",
      "Ä-Auml",
      "Å-Aring",
      "Æ-AElig",
      "Ç-Ccedil",
      "È-Egrave",
      "É-Eacute",
      "Ê-Ecirc",
      "Ë-Euml",
      "Ì-Igrave",
      "Í-Iacute",
      "Î-Icirc",
      "Ï-Iuml",
      "Ð-ETH",
      "Ñ-Ntilde",
      "Ò-Ograve",
      "Ó-Oacute",
      "Ô-Ocirc",
      "Õ-Otilde",
      "Ö-Ouml",
      "×-times",
      "Ø-Oslash",
      "Ù-Ugrave",
      "Ú-Uacute",
      "Û-Ucirc",
      "Ü-Uuml",
      "Ý-Yacute",
      "Þ-THORN",
      "ß-szlig",
      "à-agrave",
      "á-aacute",
      "â-acirc",
      "ã-atilde",
      "ä-auml",
      "å-aring",
      "æ-aelig",
      "ç-ccedil",
      "è-egrave",
      "é-eacute",
      "ê-ecirc",
      "ë-euml",
      "ì-igrave",
      "í-iacute",
      "î-icirc",
      "ï-iuml",
      "ð-eth",
      "ñ-ntilde",
      "ò-ograve",
      "ó-oacute",
      "ô-ocirc",
      "õ-otilde",
      "ö-ouml",
      "÷-divide",
      "ø-oslash",
      "ù-ugrave",
      "ú-uacute",
      "û-ucirc",
      "ü-uuml",
      "ý-yacute",
      "þ-thorn",
      "ÿ-yuml",
      "Œ-OElig",
      "œ-oelig",
      "Š-Scaron",
      "š-scaron",
      "Ÿ-Yuml",
      "ƒ-fnof",
      "\x02C6-circ",
      "˜-tilde",
      "Α-Alpha",
      "Β-Beta",
      "Γ-Gamma",
      "Δ-Delta",
      "Ε-Epsilon",
      "Ζ-Zeta",
      "Η-Eta",
      "Θ-Theta",
      "Ι-Iota",
      "Κ-Kappa",
      "Λ-Lambda",
      "Μ-Mu",
      "Ν-Nu",
      "Ξ-Xi",
      "Ο-Omicron",
      "Π-Pi",
      "Ρ-Rho",
      "Σ-Sigma",
      "Τ-Tau",
      "Υ-Upsilon",
      "Φ-Phi",
      "Χ-Chi",
      "Ψ-Psi",
      "Ω-Omega",
      "α-alpha",
      "β-beta",
      "γ-gamma",
      "δ-delta",
      "ε-epsilon",
      "ζ-zeta",
      "η-eta",
      "θ-theta",
      "ι-iota",
      "κ-kappa",
      "λ-lambda",
      "μ-mu",
      "ν-nu",
      "ξ-xi",
      "ο-omicron",
      "π-pi",
      "ρ-rho",
      "ς-sigmaf",
      "σ-sigma",
      "τ-tau",
      "υ-upsilon",
      "φ-phi",
      "χ-chi",
      "ψ-psi",
      "ω-omega",
      "ϑ-thetasym",
      "ϒ-upsih",
      "ϖ-piv",
      " -ensp",
      " -emsp",
      " -thinsp",
      "\x200C-zwnj",
      "\x200D-zwj",
      "\x200E-lrm",
      "\x200F-rlm",
      "–-ndash",
      "—-mdash",
      "‘-lsquo",
      "’-rsquo",
      "‚-sbquo",
      "“-ldquo",
      "”-rdquo",
      "„-bdquo",
      "†-dagger",
      "‡-Dagger",
      "•-bull",
      "…-hellip",
      "‰-permil",
      "′-prime",
      "″-Prime",
      "‹-lsaquo",
      "›-rsaquo",
      "‾-oline",
      "⁄-frasl",
      "€-euro",
      "ℑ-image",
      "℘-weierp",
      "ℜ-real",
      "™-trade",
      "ℵ-alefsym",
      "←-larr",
      "↑-uarr",
      "→-rarr",
      "↓-darr",
      "↔-harr",
      "↵-crarr",
      "⇐-lArr",
      "⇑-uArr",
      "⇒-rArr",
      "⇓-dArr",
      "⇔-hArr",
      "∀-forall",
      "∂-part",
      "∃-exist",
      "∅-empty",
      "∇-nabla",
      "∈-isin",
      "∉-notin",
      "∋-ni",
      "∏-prod",
      "∑-sum",
      "−-minus",
      "∗-lowast",
      "√-radic",
      "∝-prop",
      "∞-infin",
      "∠-ang",
      "∧-and",
      "∨-or",
      "∩-cap",
      "∪-cup",
      "∫-int",
      "∴-there4",
      "∼-sim",
      "≅-cong",
      "≈-asymp",
      "≠-ne",
      "≡-equiv",
      "≤-le",
      "≥-ge",
      "⊂-sub",
      "⊃-sup",
      "⊄-nsub",
      "⊆-sube",
      "⊇-supe",
      "⊕-oplus",
      "⊗-otimes",
      "⊥-perp",
      "⋅-sdot",
      "⌈-lceil",
      "⌉-rceil",
      "⌊-lfloor",
      "⌋-rfloor",
      "〈-lang",
      "〉-rang",
      "◊-loz",
      "♠-spades",
      "♣-clubs",
      "♥-hearts",
      "♦-diams"
    };
			private static Hashtable _entitiesLookupTable;

			static HtmlEntities()
			{
			}

			private HtmlEntities()
			{
			}

			internal static char Lookup(string entity)
			{
				if (HtmlEntities._entitiesLookupTable == null)
				{
					lock (HtmlEntities._lookupLockObject)
					{
						if (HtmlEntities._entitiesLookupTable == null)
						{
							Hashtable local_0 = new Hashtable();
							foreach (string item_0 in HtmlEntities._entitiesList)
								local_0[(object)item_0.Substring(2)] = (object)item_0[0];
							HtmlEntities._entitiesLookupTable = local_0;
						}
					}
				}
				object obj = HtmlEntities._entitiesLookupTable[(object)entity];
				if (obj != null)
					return (char)obj;
				else
					return char.MinValue;
			}
		}
		
		#endregion

	}

}
