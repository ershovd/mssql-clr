using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Implementation for stored procedure executing when insert process ends.
	/// </summary>
	[Serializable]
	public class SQLConnectionProcessEnd : SQLConnectionProxy
	{
		public SQLConnectionProcessEnd(int processRecId, string errors)
		{
			SQLParameters = CreateEndProcessParameters(processRecId, errors);
		}

		private static IEnumerable<SqlParameter> CreateEndProcessParameters(int processRecId, string errors)
		{
			yield return new SqlParameter("@ProcessRecID", SqlDbType.Int)
			{
				Value = processRecId
			};
			yield return new SqlParameter("@ErrorsDetected", SqlDbType.NVarChar, -1)
			{
				Value = ConvertToDb(errors)
			};
		}

	}
}
