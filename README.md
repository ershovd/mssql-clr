# README #

This README contains information regarding SQL CLR stored procedures for reading AD structure ad web-service XML data and inserting this data into SQL Server

### Overview ###

The solution contains ActiveDirectoryProcedures - main project used for defining stored procedures and infrastructure for reading AD and webservices. ActiveDirectoryProcedures.Console used for internal testing and can be ignored.
The ActiveDirectoryProcedures project contain definition for 2 stored procedures: ReadADUsersAndUserGroups and ReadXMLWebService (in ActiveDirectoryProcedures.StoredProcedures class)

### Developers notes ###

* The project targets SQL server 2012 and .Net 4.0. This can be change in project properties -> Project Properties and SQLCLR tabs.
* The System.DirectoryServices referenced assembly must be added to SQL server specifically, see installation notes below.
* Take a look at SQLConnectionProxy.ConvertToDb method. This is used to convert from .Net to SQL Server types. Special care should be taken when converting to SqlDateTime and SQL GUID type.
* Properties for reading AD attributes are defined in ADProperties class. In PropertyItem class the mapping between AD entity and SQL data type is defined.
* AD related functions reside in ActiveDirectorySearcher class. 
* Web-service reading functions are in WebServiceReader class
* In order to debug .Net code of SQL CLR procedure you need to enable debugging at: VS Studio-> View -> SQL Server object explorer -> Your SQL Server (right-click) -> Allow SQL\CLR Debugging
### Installation and deployment ###
Deploying SQL CLR procedures requires several steps:

* You need to enable CLR execution and set TRUSTWORHY server on:

```
#!sql

sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO
sp_configure 'clr enabled', 1;
GO
RECONFIGURE;
GO

ALTER DATABASE DatabaseName SET TRUSTWORTHY ON;
GO

```
* Then you need to make sure that assembly System.DirectoryServices exists on the SQL catalog. Make sure that .Net framework specified matched the CLR version installed on SQL Server:

```
#!sql
	CREATE ASSEMBLY [System.DirectoryServices]
	FROM 'C:\Windows\Microsoft.NET\Framework64\v4.0.30319\System.DirectoryServices.dll'
	WITH PERMISSION_SET = UNSAFE

```
* Then you should create the target .Net assembly with stored procedure with sample script: (note: you can get the exact assembly binary values when you run the project in Visual studio (in ActiveDirectoryProcedures/bin/Debug/ActiveDirectoryProcedures.sql))


```
#!sql

use [CASYNC]  -- whatever you DB name
go


CREATE ASSEMBLY [ActiveDirectoryProcedures]
    AUTHORIZATION [dbo]  
    FROM 0x4D5... -- here goes the binary
    with permission_set = unsafe;
GO
ALTER ASSEMBLY [ActiveDirectoryProcedures]
    DROP FILE ALL
    ADD FILE FROM 0x4D6963726F... /* this is for debugging */ .000 AS N'ActiveDirectoryProcedures.pdb';
GO

-- this is for reading Active directory data
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[ReadADUsersAndUserGroups]
	@domainName [nvarchar](max),
	@domainUserId [nvarchar](max),
	@domainPassword [nvarchar](max),
	@isBatch [bit],
	@entityRecID [int],
	@processDescription [nvarchar](max),
	@canonicalName [nvarchar](max)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [ActiveDirectoryProcedures].[ActiveDirectoryProcedures.StoredProcedures].[ReadADUsersAndUserGroups]
GO

-- this is for reading webservices
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[ReadXMLWebService]
	@entityRecId [int],
	@externalSourceRecID [int],
	@externalSourceKey [nvarchar](max),
	@sourceURLCalled [nvarchar](max)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [ActiveDirectoryProcedures].[ActiveDirectoryProcedures.StoredProcedures].[ReadXMLWebService]
GO

```

* After that you can call the stored procedures with provided parameters, like this:

```
#!sql

exec [CASYNC].[dbo].ReadADUsersAndUserGroups 'LDAP://rdc1.rlabs.local/DC=rlabs,DC=local' ,'cdev', 'YourPassword', true, 42, 'Some description', 'cdev'

exec [CASYNC].[dbo].ReadXMLWebService 1, 1, null, 'http://www.webservicex.com/globalweather.asmx/GetWeather?CityName=Sydney&CountryName=Australia' 

go

```
