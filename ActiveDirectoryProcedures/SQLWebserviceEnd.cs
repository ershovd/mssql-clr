using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	///  Implementation for stored procedure executing when reading of webservice ends
	/// </summary>
	[Serializable]
	public class SQLWebserviceEnd : SQLConnectionProxy
	{
		public SQLWebserviceEnd(int webServiceReceiverRecID, string sourceXMLData, string errorMessages)
		{
			SQLParameters = CreateWebserviceEndParamters(webServiceReceiverRecID, sourceXMLData, errorMessages);
		}

		private IEnumerable<SqlParameter> CreateWebserviceEndParamters(int webServiceReceiverRecID, string sourceXMLData, string errorMessages)
		{
			yield return new SqlParameter("@WebServiceReceiverRecID", SqlDbType.Int)
			{
				Value = webServiceReceiverRecID
			};
			yield return new SqlParameter("@SourceXMLData", SqlDbType.NVarChar, -1)
			{
				Value = ConvertToDb(sourceXMLData)
			};
			yield return new SqlParameter("@ErrorMessages", SqlDbType.NVarChar, -1)
			{
				Value = ConvertToDb(errorMessages)
			};
		}
	}
}
