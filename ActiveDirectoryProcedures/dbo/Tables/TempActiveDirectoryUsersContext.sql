﻿CREATE TABLE [dbo].[TempActiveDirectoryUsersContext] (
    [ContextRecID]   INT            IDENTITY (1, 1) NOT NULL,
    [ProcessRecID]   INT            NOT NULL,
    [ADRecID]        INT            NULL,
    [SamAccountName] NVARCHAR (100) NULL,
    [Context]        NVARCHAR (50)  NULL,
    [ContextData]    NVARCHAR (500) NULL,
    CONSTRAINT [PK_TempActiveDirectoryUsersContext] PRIMARY KEY CLUSTERED ([ContextRecID] ASC)
);

