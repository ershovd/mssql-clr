﻿Create procedure dbo.spu_ActiveDirectoryProcessHeader_End(@ProcessRecID INT , @ErrorsDetected nvarchar(max))
AS
BEGIN
UPDATE [dbo].[ActiveDirectoryProcessHeader]
SET [EndDateTime] = GetDate()
,[ErrorsDetected] = IsNull(@ErrorsDetected,'')
WHERE ProcessRecID = @ProcessRecID
/* This is where we write code to transfer the Temp Directory data to the current data
*/
END
RETURN 0