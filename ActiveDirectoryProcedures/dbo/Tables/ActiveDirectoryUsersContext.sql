﻿CREATE TABLE [dbo].[ActiveDirectoryUsersContext] (
    [ContextRecID]            INT            IDENTITY (1, 1) NOT NULL,
    [ADRecID]                 INT            NOT NULL,
    [SamAccountName]          NVARCHAR (100) NULL,
    [Context]                 NVARCHAR (50)  NULL,
    [ContextData]             NVARCHAR (500) NULL,
    [EntityRecID]             INT            NULL,
    [CreatedByProcessRecID]   INT            NULL,
    [LastUpdatedProcessRecID] INT            NULL,
    [ActiveFlag]              BIT            NULL,
    CONSTRAINT [PK_ActiveDirectoryUsersContext] PRIMARY KEY CLUSTERED ([ContextRecID] ASC)
);

