﻿CREATE TABLE [dbo].[WebServiceReceiver] (
    [WebServiceReceiverRecID] INT            IDENTITY (1, 1) NOT NULL,
    [EntityRecID]             INT            NOT NULL,
    [ExternalSourceRecID]     INT            NULL,
    [ExternalSourceKey]       NVARCHAR (50)  NULL,
    [SourceURLCalled]         NVARCHAR (500) NULL,
    [SourceXMLData]           NVARCHAR (MAX) NULL,
    [DateTimeStarted]         DATETIME       NULL,
    [DateTimeCompleted]       DATETIME       NULL,
    [ErrorMessages]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_WebServiceReceiver] PRIMARY KEY CLUSTERED ([WebServiceReceiverRecID] ASC)
);

