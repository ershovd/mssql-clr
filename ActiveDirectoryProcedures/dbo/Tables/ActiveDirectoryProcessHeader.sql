﻿CREATE TABLE [dbo].[ActiveDirectoryProcessHeader] (
    [ProcessRecID]       INT             IDENTITY (1, 1) NOT NULL,
    [EntityRecID]        INT             NULL,
    [StartDateTime]      DATETIME        NULL,
    [EndDateTime]        DATETIME        NULL,
    [ProcessDescription] NVARCHAR (1000) NULL,
    [ErrorsDetected]     NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_ActiveDirectoryProcessHeader] PRIMARY KEY CLUSTERED ([ProcessRecID] ASC)
);

