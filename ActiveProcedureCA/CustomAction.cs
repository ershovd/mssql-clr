﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Deployment.WindowsInstaller;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Win32;
using View = Microsoft.Deployment.WindowsInstaller.View;

namespace ActiveProcedureCA
{
	public static class CustomActions
	{
		private static string databaseServer = "SQLSERVERNAME";
		private static string databaseCatalog = "SQLDATABASE";
		private static string databaseUsername = "SQLUSERNAME";
		private static string databasePassword = "SQLUSERPASSWORD";
		private static string odbcConnectionEstablished = "ODBC_CONNECTION_ESTABLISHED";
		private static string databaseLogonType = "SQLWINAUTHORIZATION";
		private static string netFrameworkPath = "NET_FRAMEWORK_PATH";
		private static string odbcError = "ODBC_ERROR";
		private static string softwareSyntechkey = "SOFTWARE\\Syntech\\ActiveDirectory.SQL.CLR";
		private static readonly string baseFrameworkPath = @"C:\Windows\Microsoft.NET\Framework64\{0}";
		private static readonly string framework2 = string.Format(baseFrameworkPath, "v2.0.50727");
		private static readonly string framework4 = string.Format(baseFrameworkPath, "v4.0.30319");
		
		#region Public Methods and Operators

		[CustomAction]
		public static ActionResult EnumerateSqlServers(Session session)
		{
			if (null == session)
			{
				throw new ArgumentNullException("session");
			}

			session.Log("EnumerateSQLServers: Begin");


			var rows = GetFromRegistry();
			ActionResult result = EnumSqlServersIntoComboBox(session, rows);

			session.Log("EnumerateSQLServers: End");
			return result;
		}

		[CustomAction]
		public static ActionResult SetFramworkBasedOnServerName(Session session)
		{
			if (session == null) throw new ArgumentNullException("session");
			session.Log("SetFramworkBasedOnServerName: start");
			string serverName = session[databaseServer];
			session.Log(string.Format("SetFramworkBasedOnServerName: servername: {0}", serverName));
			
			session.Log("SetFramworkBasedOnServerName: GetServerVersionByName");
			var serverVersion = GetServerVersionByName(serverName);
			session.Log(string.Format("SetFramworkBasedOnServerName: serverVersion: {0}", serverVersion));

			session.Log("SetFramworkBasedOnServerName: GetFrameworkPathBySQLVersion");
			var frameworkVersion = GetFrameworkPathBySQLVersion(serverVersion);
			session[netFrameworkPath] = frameworkVersion;
			session.Log(string.Format("SetFramworkBasedOnServerName: frameworkVersion: {0}", frameworkVersion));
			
			session.Log("SetFramworkBasedOnServerName: end");
			return ActionResult.Success;
		}

		[CustomAction]
		public static ActionResult VerifySqlConnection(Session session)
		{
			try
			{
				session.Log("VerifySqlConnection: Begin");

				var userCatalog = session[databaseCatalog];
				var builder = new SqlConnectionStringBuilder
				{
					DataSource = session[databaseServer],
					InitialCatalog = string.IsNullOrEmpty(userCatalog) ? "master" : userCatalog,
					ConnectTimeout = 5
				};

				var integratedValue = "1"; //"DatabaseIntegratedAuth";
				if (session[databaseLogonType] == integratedValue)
				{
					builder.IntegratedSecurity = true;
				}
				else
				{
					builder.UserID = session[databaseUsername];
					builder.Password = session[databasePassword];
				}

				using (var connection = new SqlConnection(builder.ConnectionString))
				{
					if (connection.CheckConnection(session))
					{
						session[odbcConnectionEstablished] = "1";
						session.Log("VerifySqlConnection: established success");
					}
					else
					{
						session.Log("VerifySqlConnection: establishing failed");
						session[odbcConnectionEstablished] = string.Empty;
					}
				}

				session.Log("VerifySqlConnection: End");
			}
			catch (Exception ex)
			{
				session.Log("VerifySqlConnection: exception: {0}", ex.Message);
				throw;
			}

			return ActionResult.Success;
		}

		#endregion

		#region Methods

		private static string GetFrameworkPathBySQLVersion(string version)
		{
			if (string.IsNullOrEmpty(version)) return framework2;
			
			const int lengthOfMajorNumber = 2;

			if (version.Length > lengthOfMajorNumber)
			{
				string majorNumberStr = version.Substring(0, lengthOfMajorNumber);
				int majorVersion;
				if (int.TryParse(majorNumberStr, out majorVersion))
				{
					if (majorVersion >= 11)
					{
						return framework4;
					}
				}
			}

			return framework2;
		}

		private static IEnumerable<string> GetFromRegistry()
		{
			List<string> servers = new List<string>();

			// Get servers from the registry (if any)
			RegistryKey key = GetBaseRegistry();
			key = key.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
			object installedInstances = null;
			if (key != null) { installedInstances = key.GetValue("InstalledInstances"); }
			List<string> instances = null;
			if (installedInstances != null) { instances = ((string[])installedInstances).ToList(); }
			if (System.Environment.Is64BitOperatingSystem)
			{
				/* The above registry check gets routed to the syswow portion of 
				 * the registry because we're running in a 32-bit app. Need 
				 * to get the 64-bit registry value(s) */
				key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
				key = key.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
				installedInstances = null;
				if (key != null)
				{
					installedInstances = key.GetValue("InstalledInstances");
				}
				string[] moreInstances = null;
				if (installedInstances != null)
				{
					moreInstances = (string[])installedInstances;
					if (instances == null)
					{
						instances = moreInstances.ToList();
					}
					else
					{
						instances.AddRange(moreInstances);
					}
				}
			}
			if (instances != null)
			{
				foreach (string item in instances)
				{
					string name = Environment.MachineName;
					if (item != "MSSQLSERVER") { name += @"\" + item; }
					if (!servers.Contains(name.ToUpper())) { servers.Add(name.ToUpper()); }
				}
			}

			return servers;
		}

		private static string GetServerVersionByName(string serverName)
		{
			if (serverName == null) throw new ArgumentNullException("serverName");

			int indexOf = serverName.IndexOf('\\');
			string instanceName = indexOf < 0 ? serverName : serverName.Substring(indexOf + 1);

			using (var baseKey = GetBaseRegistry())
			{
				using (var currKey = baseKey.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\" + instanceName + @"\MSSQLServer\CurrentVersion"))
				{
					if (currKey != null)
					{
						var value = currKey.GetValue("CurrentVersion");
						if (value != null)
						{
							return value.ToString();
						}
					}
				}
			}

			return string.Empty;
		}

		private static void StartDebug()
		{
			if (Debugger.IsAttached)
			{
				Debugger.Break();
			}
			else
			{
				Debugger.Launch();
			}
		}

		private static RegistryKey GetBaseRegistry()
		{
			RegistryView registryView = Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32;
			return RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView);
		}
		
		private static ActionResult EnumSqlServersIntoComboBox(Session session, IEnumerable<string> rows)
		{
			try
			{
				//Debugger.Break();

				session.Log("EnumSQLServers: Begin");

				View view = session.Database.OpenView(string.Format("DELETE FROM ComboBox WHERE ComboBox.Property='{0}'", databaseServer));
				view.Execute();

				view = session.Database.OpenView("SELECT * FROM ComboBox");
				view.Execute();

				Int32 index = 1;
				session.Log("EnumSQLServers: Enumerating SQL servers");
				foreach (var row in rows)
				{
					String serverName = row;

					// Create a record for this web site. All I care about is
					// the name so use it for fields three and four.
					session.Log("EnumSQLServers: Processing SQL server: {0}", serverName);

					Record record = session.Database.CreateRecord(4);
					record.SetString(1, databaseServer);
					record.SetInteger(2, index);
					record.SetString(3, serverName);
					record.SetString(4, serverName);

					session.Log("EnumSQLServers: Adding record");
					view.Modify(ViewModifyMode.InsertTemporary, record);
					index++;
				}

				view.Close();

				session.Log("EnumSQLServers: End");
			}
			catch (Exception ex)
			{
				session.Log("EnumSQLServers: exception: {0}", ex.Message);
				throw;
			}

			return ActionResult.Success;
		}

		private static bool CheckConnection(this SqlConnection connection, Session session)
		{
			try
			{
				if (connection == null)
				{
					return false;
				}

				connection.Open();
				var canOpen = connection.State == ConnectionState.Open;
				connection.Close();

				return canOpen;
			}
			catch (SqlException ex)
			{
				var message = ex.Message;
				session[odbcError] = message;
				session.Log("VerifySqlConnection: failed: {0}", message);
				return false;
			}
		}


		#endregion
	}
}
