﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*
USE ActiveDirectory;
if NOT Exists (select * from sys.assemblies where name like 'System.DirectoryServices')
begin
	CREATE ASSEMBLY [System.DirectoryServices]
	FROM 'C:\Windows\Microsoft.NET\Framework64\v4.0.30319\System.DirectoryServices.dll'
	WITH PERMISSION_SET = UNSAFE
end
GO

USE ActiveDirectory;
if NOT Exists (select * from sys.assemblies where name like 'System.Web')
begin
	CREATE ASSEMBLY [System.Web]
	FROM 'C:\Windows\Microsoft.NET\Framework64\v4.0.30319\System.Web.dll'
	WITH PERMISSION_SET = UNSAFE
end
GO
*/