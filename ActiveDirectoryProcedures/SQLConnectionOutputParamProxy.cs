using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
//using System.Linq;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Implementation for accessing output parameter in proxy class
	/// </summary>
	[Serializable]
	public class SQLConnectionOutputParamProxy : SQLConnectionProxy
	{
		private object returnParameter = null;
		
		/// <summary>
		/// Returned value of the single output paramter
		/// </summary>
		public object OutputParameter
		{
			get
			{
				if (returnParameter == null)
				{
					var parameter = SQLCommand.Parameters.Cast<SqlParameter>().FirstOrDefault(f => f.Direction == ParameterDirection.Output);
					if (parameter != null)
						returnParameter = parameter.Value;
				}
				return returnParameter;
			}
		}
	}
}
