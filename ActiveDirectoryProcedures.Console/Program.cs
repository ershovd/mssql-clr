﻿using System;
using System.Data.SqlTypes;
using System.Runtime.InteropServices;

namespace ActiveDirectoryProcedures
{
	class Program
	{
		static void Main(string[] args)
		{
			var url = new SqlString(@"https://assist.lauriston.vic.edu.au/collaborate/test/testget.asp");
			var user = new SqlString(@"install");
			var passwd = new SqlString(@"Shrubb3ry");
			var domain = new SqlString(@"Lauriston");

			SqlString requestRes = StoredProcedures.CaSyncGetWebPage(url, user, passwd, domain);
			Console.WriteLine(requestRes.ToString());
			
			Console.ReadLine();
		}
	}
}
