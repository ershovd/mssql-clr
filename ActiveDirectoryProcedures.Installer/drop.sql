USE ActiveDirectory
GO
/*** STORED PROCS **/
-- DROP PROCEDURE [ReadADUsersAndUserGroups];
DROP PROCEDURE [ReadXMLWebService];
GO

DROP FUNCTION [CaSyncGetWebPage];
GO

DROP FUNCTION [CaSyncPostWebPage];
GO
/*** Assembly **/
-- DROP ASSEMBLY [System.DirectoryServices];
DROP ASSEMBLY [ActiveDirectoryProcedures];
GO