using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Implementation for stored procedure executing when insert process starts.
	/// </summary>
	[Serializable]
	public class SQLConnectionProcessStart : SQLConnectionOutputParamProxy
	{
		public SQLConnectionProcessStart(int entityRecID, string processDescription)
		{
			SQLParameters = CreateStartProcessParameters(entityRecID, processDescription);
		}

		private static IEnumerable<SqlParameter> CreateStartProcessParameters(int entityRecID, string processDescription)
		{
			yield return new SqlParameter("@EntityRecID", SqlDbType.Int)
				             {
					             Value = entityRecID
				             };
			yield return new SqlParameter("@ProcessDescription", SqlDbType.NVarChar, 1000)
				             {
								 Value = ConvertToDb(processDescription)
				             };
			yield return new SqlParameter("@ProcessRecID", SqlDbType.Int)
				             {
					             Direction = ParameterDirection.Output
				             };
		}
	}
}