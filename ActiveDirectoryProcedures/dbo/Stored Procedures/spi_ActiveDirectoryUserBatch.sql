﻿Create Procedure [dbo].[spi_ActiveDirectoryUserBatch]
(@ProcessRecID int
,@AccountLockoutTime datetime
,@CannotChangePassword bit
,@CanonicalName nvarchar(1000)
,@City nvarchar(100)
,@CN nvarchar(100)
,@Company nvarchar(100)
,@Country nvarchar(100)
,@countryCode int
,@Created datetime
,@createTimeStamp datetime
,@Deleted bit
,@Department nvarchar(100)
,@Description nvarchar(1000)
,@DisplayName nvarchar(1000)
,@DistinguishedName nvarchar(1000)
,@Division nvarchar(100)
,@EmailAddress nvarchar(200)
,@EmployeeID nvarchar(100)
,@EmployeeNumber int
,@Enabled bit
,@Fax nvarchar(100)
,@GivenName nvarchar(100)
,@HomeDirectory nvarchar(1000)
,@HomedirRequired bit
,@HomeDrive nvarchar(100)
,@HomePage nvarchar(200)
,@HomePhone nvarchar(100)
,@Initials nvarchar(50)
,@LastBadPasswordAttempt datetime
,@LastLogonDate datetime
,@LockedOut bit
,@logonCount int
,@Manager nvarchar(100)
,@MobilePhone nvarchar(100)
,@Modified datetime
,@Name nvarchar(1000)
,@ObjectClass nvarchar(100)
,@ObjectGUID nvarchar(100)
,@Office nvarchar(100)
,@OfficePhone nvarchar(100)
,@Organization nvarchar(100)
,@OtherName nvarchar(100)
,@PasswordExpired bit
,@PasswordLastSet datetime
,@PasswordNeverExpires bit
,@PasswordNotRequired bit
,@POBox nvarchar(100)
,@PostalCode nvarchar(100)
,@SamAccountName nvarchar(100)
,@sAMAccountType int
,@ScriptPath nvarchar(1000)
,@SmartcardLogonRequired bit
,@State nvarchar(100)
,@StreetAddress nvarchar(1000)
,@Surname nvarchar(100)
,@Title nvarchar(100)
,@userAccountControl int
,@UserPrincipalName nvarchar(100))
AS
BEGIN
DECLARE @WkObjectGUID Uniqueidentifier; --@WkObjectGUIDUniqueIdentifier
Select @WkObjectGUID = Cast(@ObjectGUID as Uniqueidentifier)
INSERT INTO [dbo].[TempActiveDirectoryUser]
([ProcessRecID]
,[AccountLockoutTime]
,[CannotChangePassword]
,[CanonicalName]
,[City]
,[CN]
,[Company]
,[Country]
,[countryCode]
,[Created]
,[createTimeStamp]
,[Deleted]
,[Department]
,[Description]
,[DisplayName]
,[DistinguishedName]
,[Division]
,[EmailAddress]
,[EmployeeID]
,[EmployeeNumber]
,[Enabled]
,[Fax]
,[GivenName]
,[HomeDirectory]
,[HomedirRequired]
,[HomeDrive]
,[HomePage]
,[HomePhone]
,[Initials]
,[LastBadPasswordAttempt]
,[LastLogonDate]
,[LockedOut]
,[logonCount]
,[Manager]
,[MobilePhone]
,[Modified]
,[Name]
,[ObjectClass]
,[ObjectGUID]
,[Office]
,[OfficePhone]
,[Organization]
,[OtherName]
,[PasswordExpired]
,[PasswordLastSet]
,[PasswordNeverExpires]
,[PasswordNotRequired]
,[POBox]
,[PostalCode]
,[SamAccountName]
,[sAMAccountType]
,[ScriptPath]
,[SmartcardLogonRequired]
,[State]
,[StreetAddress]
,[Surname]
,[Title]
,[userAccountControl]
,[UserPrincipalName])
VALUES
(@ProcessRecID
,@AccountLockoutTime
,@CannotChangePassword
,@CanonicalName
,@City
,@CN
,@Company
,@Country
,@countryCode
,@Created
,@createTimeStamp
,@Deleted
,@Department
,@Description
,@DisplayName
,@DistinguishedName
,@Division
,@EmailAddress
,@EmployeeID
,@EmployeeNumber
,@Enabled
,@Fax
,@GivenName
,@HomeDirectory
,@HomedirRequired
,@HomeDrive
,@HomePage
,@HomePhone
,@Initials
,@LastBadPasswordAttempt
,@LastLogonDate
,@LockedOut
,@logonCount
,@Manager
,@MobilePhone
,@Modified
,@Name
,@ObjectClass
,@WkObjectGUID
,@Office
,@OfficePhone
,@Organization
,@OtherName
,@PasswordExpired
,@PasswordLastSet
,@PasswordNeverExpires
,@PasswordNotRequired
,@POBox
,@PostalCode
,@SamAccountName
,@sAMAccountType
,@ScriptPath
,@SmartcardLogonRequired
,@State
,@StreetAddress
,@Surname
,@Title
,@userAccountControl
,@UserPrincipalName)
END
RETURN 0
