﻿Create Procedure [dbo].[spi_WebServiceReceiverStart] (@EntityRecID int
,@ExternalSourceRecID int
,@ExternalSourceKey nvarchar(50)
,@SourceURLCalled nvarchar(500)
,@WebServiceReceiverRecID INT OUTPUT)
AS
BEGIN
INSERT INTO [dbo].[WebServiceReceiver]
([EntityRecID]
,[ExternalSourceRecID]
,[ExternalSourceKey]
,[SourceURLCalled]
,[SourceXMLData]
,[DateTimeStarted]
,[DateTimeCompleted]
,[ErrorMessages])
VALUES
(@EntityRecID
,@ExternalSourceRecID
,@ExternalSourceKey
,@SourceURLCalled
,NULL ,
GetDate()
,NULL
,NULL)
Select @WebServiceReceiverRecID = @@IDENTITY
END
RETURN 0
