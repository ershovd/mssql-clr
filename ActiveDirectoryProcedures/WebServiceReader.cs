using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Default implementaion of reading webservice
	/// </summary>
	[Serializable]
	public class WebServiceReader
	{
		/// <summary>
		/// Read entire webservice page and returns into XML string
		/// </summary>
		public static string Read(string sourceURLCalled)
		{
			if (string.IsNullOrEmpty(sourceURLCalled)) throw new ArgumentNullException("sourceURLCalled");

			var request = WebRequest.Create(sourceURLCalled);

			using (var response = request.GetResponse())
			{
				using (Stream stream = response.GetResponseStream())
				{
					if (stream != null)
					{
						using (var reader = new StreamReader(stream))
						{
							return reader.ReadToEnd();
						}
					}
				}
			}

			return null;
		}
	}
}
