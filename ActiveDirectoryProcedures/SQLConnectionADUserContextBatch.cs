using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	///  Implementation class for executing insert user context into UserContextBatch table
	/// </summary>
	[Serializable]
	public class SQLConnectionADUserContextBatch : SQLConnectionProxy
	{
		public SQLConnectionADUserContextBatch(ADProperties properties, KeyValuePair<string, string> attribValuePair, int processRecID)
		{
			if (properties == null) throw new ArgumentNullException("properties");
			if (processRecID < 0) throw new ArgumentOutOfRangeException("processRecID");
			
			SQLParameters = CreateInsertUserContextParameters(properties, attribValuePair, processRecID);
		}

		private static IEnumerable<SqlParameter> CreateInsertUserContextParameters(ADProperties properties, KeyValuePair<string, string> attribValuePair, int processRecID)
		{
			yield return new SqlParameter("@ProcessRecID", SqlDbType.Int)
			{
				Value = processRecID
			};
			// check that sAMAccountName is not null
			object samValue = properties.sAMAccountName.Value;
			if (samValue == null)
				throw new InvalidOperationException("sAMAccountName value could not be null!");
			yield return new SqlParameter("@SamAccountName", SqlDbType.NVarChar, 100)
			{
				Value = ConvertToDb(samValue)
			};
			yield return new SqlParameter("@Context", SqlDbType.NVarChar, 50)
			{
				// this is attribute name (e.g 'CN')
				Value = ConvertToDb(attribValuePair.Key)
			};
			yield return new SqlParameter("@ContextData", SqlDbType.NVarChar, 500)
			{
				// this is attribute value (e.g. 'myName' )
				Value = ConvertToDb(attribValuePair.Value)
			};
		}
	}
}
