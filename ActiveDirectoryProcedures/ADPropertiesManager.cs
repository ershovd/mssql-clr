using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
using System.Data.SqlClient;
using System.Reflection;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Helper class for woring with ADProperties class properties using reflection
	/// </summary>
	[Serializable]
	public static class ADPropertiesManager
	{
/*
		/// <summary>
		/// Get property name for expression with ADProperties returning PropertyItem
		/// </summary>
		public static string PropertyName(Expression<Func<ADProperties, PropertyItem>> expression)
		{
			if (expression == null) throw new ArgumentNullException("expression");

			var member = expression.Body as MemberExpression;
			if (member != null && member.Member is PropertyInfo)
			{
				var info = member.Member as PropertyInfo;
				if (info != null)
					return info.Name;
			}

			throw new ArgumentException("Expression is not a Property", "expression");
		}
*/
		/// <summary>
		/// Get Active Directory (LDAP) names of all properties of the <paramref name="propertiesItem"/> instance
		/// </summary>
		public static IEnumerable<string> GetADPropsNames(ADProperties propertiesItem)
		{
			return GetInnerPropertyItem<string>(propertiesItem, f => f.ADName);
		}

		/// <summary>
		/// Get all properties instance of all properties of the <paramref name="propertiesItem"/> instance
		/// </summary>
		/// <param name="propertiesItem"></param>
		/// <returns></returns>
		public static IEnumerable<PropertyItem> GetAllProperties(ADProperties propertiesItem)
		{
			return GetInnerPropertyItem<PropertyItem>(propertiesItem, item => item).ToList();
		}

		/// <summary>
		/// Create new instance of ADProperties and initialize all properties 
		/// </summary>
		public static ADProperties InitializeADMetadata()
		{
			var item = new ADProperties();
			foreach (PropertyInfo info in typeof(ADProperties).GetProperties())
			{
				string propertyName = info.Name;
				info.SetValue(item, new PropertyItem(propertyName), null);
			}
			return item;
		}



		private static IEnumerable<TType> GetInnerPropertyItem<TType>(ADProperties propertiesItem, PropertyItemDelegete expression)
		{
			
			IList<TType> names = new List<TType>();
			foreach (var info in propertiesItem.GetType().GetProperties())
			{
				var item = info.GetValue(propertiesItem, null) as PropertyItem;
				if (item != null)
				{
					names.Add((TType)expression(item));
				}
			}
			return names;
		}
	}
	/// <summary>
	/// Delegate for property item (emulating .Net 3.5 Func and Expression class)
	/// </summary>
	public delegate object PropertyItemDelegete(PropertyItem item);

	public delegate bool SqlParameterDelegate(SqlParameter item);

	/// <summary>
	/// Mimic collection extension from LINQ
	/// </summary>
	public static class ListExtensionsHelper
	{
		public static List<TType> ToList<TType>(this IEnumerable<TType> list)
		{
			List<TType> newList = new List<TType>();
			foreach (var item in list)
			{
				newList.Add(item);
			}
			return newList;
		}

		public static IEnumerable<TType> Cast<TType>(this IEnumerable source)
		{
			foreach (var item in source)
			{
				yield return (TType)item;
			}
		}
		
		public static TType FirstOrDefault<TType>(this IEnumerable<TType> list, SqlParameterDelegate expression)
		{
			foreach (var item in list)
			{
				var castItem = item as SqlParameter;
				if (expression(castItem))
					return item;
			}
			return default(TType);
		}

		public static TType[] ToArray<TType>(this IEnumerable<TType> list)
		{
			List<TType> items = list.ToList();

			var arr = new TType[items.Count];
			for (int i = 0; i < items.Count; i++)
			{
				arr[i] = items[i];
			}

			return arr;
		}

	}
}
// this is dirty hack from here http://stackoverflow.com/questions/783155/using-extension-methods-with-net-framework-2-0
namespace System.Runtime.CompilerServices
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class ExtensionAttribute : Attribute
	{
	}
}