using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Data.SqlClient;

namespace ActiveDirectoryProcedures
{
	/// <summary>
	/// Implementation for stored procedure executing when reading of webservice starts
	/// </summary>
	[Serializable]
	public class SQLWebserviceStart : SQLConnectionOutputParamProxy
	{
		public SQLWebserviceStart(int entityRecId, int externalSourceRecID, string externalSourceKey, string sourceURLCalled)
		{
			SQLParameters = CreateWebserviceStartParameters(entityRecId, externalSourceRecID, externalSourceKey, sourceURLCalled);
		}

		private IEnumerable<SqlParameter> CreateWebserviceStartParameters(int entityRecId, int externalSourceRecID, string externalSourceKey, string sourceURLCalled)
		{
			yield return new SqlParameter("@EntityRecID", SqlDbType.Int)
			{
				Value = entityRecId
			};
			yield return new SqlParameter("@ExternalSourceRecID", SqlDbType.Int)
			{
				Value = externalSourceRecID
			};
			yield return new SqlParameter("@ExternalSourceKey", SqlDbType.NVarChar, 50)
			{
				Value = ConvertToDb(externalSourceKey)
			};
			yield return new SqlParameter("@SourceURLCalled", SqlDbType.NVarChar, 500)
			{
				Value = ConvertToDb(sourceURLCalled)
			};
			yield return new SqlParameter("@WebServiceReceiverRecID", SqlDbType.Int)
			{
				Direction = ParameterDirection.Output
			};
		}
	}
}
