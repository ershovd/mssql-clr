﻿Create
procedure dbo.spi_ActiveDirectoryProcessHeader_Start(@EntityRecID INT,@ProcessDescription nvarchar(1000),
@ProcessRecID INT OUTPUT)
AS
BEGIN
INSERT INTO ActiveDirectoryProcessHeader
([EntityRecID]
,[StartDateTime]
,[EndDateTime]
,[ProcessDescription]
,[ErrorsDetected])
VALUES (@EntityRecID
,GetDate()
,NULL
,IsNull(@ProcessDescription,'')
,NULL )
Select @ProcessRecID = @@IDENTITY

END
RETURN 0