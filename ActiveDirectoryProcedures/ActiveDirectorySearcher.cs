using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
//using System.DirectoryServices;
//using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ActiveDirectoryProcedures
{
	/*
	/// <summary>
	/// Class for retrieve data thru Active directory
	/// </summary>
	[Serializable]
	public class ActiveDirectorySearcher
	{
		/// <summary>
		/// Char separator for user group info
		/// </summary>
		private static readonly string valueSeparator = ";";
		/// <summary>
		/// Default page size when getting AD data
		/// </summary>
		private static readonly int defaultPageSize = 1000;

		/// <summary>
		/// Get DirectoryEntry for specified <paramref name="adRoot"/>, <paramref name="userName"/> and <paramref name="password"/>
		/// </summary>
		private DirectoryEntry GetRootEntry(string adRoot, string userName, string password)
		{
			if (userName == null)
			{
				return new DirectoryEntry(adRoot);
			}
			
			return new DirectoryEntry(adRoot, userName, password);
		}

		/// <summary>
		/// Get all users and groups for specified <paramref name="adRoot"/>, <paramref name="userName"/> and <paramref name="password"/> with optional <paramref name="canonicalName"/> for user
		/// </summary>
		public IList<ADProperties> SearchAD(string adRoot, string userName, string password, string canonicalName = null)
		{
			IList<ADProperties> propertyList = new List<ADProperties>();
			ADProperties adProperties = ADPropertiesManager.InitializeADMetadata();
			string[] properties = ADPropertiesManager.GetADPropsNames(adProperties).ToArray();

			try
			{
				using (DirectoryEntry rootEntry = GetRootEntry(adRoot, userName, password))
				{
					//Create a directory searcher with aproperiate filter, properties and search scope
					string baseFilter = "(|(objectClass=group)(objectClass=user))";
					if (canonicalName != null)
					{
						baseFilter = string.Format("(&(cn={0})", canonicalName) + baseFilter + ")";
					}
					using (DirectorySearcher ds = new DirectorySearcher(rootEntry, baseFilter, properties, SearchScope.Subtree))
					{
						ds.PageSize = SetPageSize(defaultPageSize, 0);
						//Set Page Size - without this we will not do a paged search and we will be limiited to 1000 results

						//find all object from the rood, according the filter and search scope
						using (SearchResultCollection results = ds.FindAll())
						{
							foreach (SearchResult result in results)
							{
								ADProperties aciveDirEntry = ADPropertiesManager.InitializeADMetadata();

								foreach (PropertyItem propertyItem in ADPropertiesManager.GetAllProperties(aciveDirEntry))
								{
									string adName = propertyItem.ADName;

									if (result.Properties.Contains(adName))
									{
										ResultPropertyValueCollection valueCollection = result.Properties[adName];
										if (valueCollection.Count > 0)
										{
											propertyItem.Value = ProcessValueCollection(valueCollection);
										}
									}
								}

								propertyList.Add(aciveDirEntry);
							}

						}
					}
				}
			}
			catch (COMException ex)
			{
				// handle specific COMException into user-friendly format
				if (ex.ErrorCode == -2147467259)
				{
					throw new InvalidOperationException("The LDAP path is not correctly formated. LDAP path format should match LDAP following format https://msdn.microsoft.com/en-us/library/windows/desktop/aa746384(v=vs.85).aspx.");
				}
				if (ex.ErrorCode == -2147016689)
				{
					throw new InvalidOperationException("The Active Directory service is not available. Please check the AD credentials and service is up and running.");
				}
				if (ex.ErrorCode == -2147463168)
				{
					throw new InvalidOperationException("The specified resource is not accessible in Active Directory. Please check AD access credentials and resource path.");
				}
				
				throw;
			}

			return propertyList;
		}

		/// <summary>
		/// Handles case when collection contains multiple elements
		/// </summary>
		/// <param name="valueCollection"></param>
		/// <returns></returns>
		private static object ProcessValueCollection(ResultPropertyValueCollection valueCollection)
		{
			if (valueCollection.Count == 1)
				return valueCollection[0];

			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < valueCollection.Count; i++)
			{
				var val = valueCollection[i];
				builder.Append(val);
				if (i < valueCollection.Count - 1)
				{
					builder.Append(valueSeparator);
				}
			}

			return builder.ToString();
		}

		private int SetPageSize(int pageSize, SqlInt32 rowsLimit)
		{
			int limit = rowsLimit.IsNull ? 0 : rowsLimit.Value;
			if (rowsLimit > 0 && pageSize > limit)
				pageSize = limit;
			return pageSize;
		}

		/// <summary>
		/// Split distinguished name value into list of keu=value pairs
		/// </summary>
		public IEnumerable<KeyValuePair<string, string>> SplitDistinguishedNameIntoDict(ADProperties property)
		{
			if (property == null) throw new ArgumentNullException("property");
			object value = property.DistinguishedName.Value;
			if (value == null) throw new ArgumentNullException("property");
			return SplitDistinguishedNameIntoDict(value.ToString());
		}
		
		private IEnumerable<KeyValuePair<string, string>> SplitDistinguishedNameIntoDict(string input)
		{
			int index = 0;
			int attrIndex = 0;
			int valIndex = 0;
			var attribute = new char[50];
			var value = new char[200];
			var inAttribute = true; // true if current chat inside attribute definition
			var names = new List<KeyValuePair<string, string>>();

			while (index < input.Length)
			{
				char ch = input[index++];
				switch(ch)
				{
					case '\\':  // work around escape characters
						value[valIndex++] = ch;
						value[valIndex++] = input[index++];
						break;
					case '=':  // separator of key=value
						inAttribute = false;
						break;
					case ',':  // separator between items
						inAttribute = true;
						names.Add(CreatePairFromVals(attribute, attrIndex, value, valIndex));
						attrIndex = valIndex = 0;
						break;
					default:
						if (inAttribute)
						{
							attribute[attrIndex++] = ch;
						}
						else
						{
							value[valIndex++] = ch;
						}
						break;
				}
			}
			// this is required for last key=value pair
			names.Add(CreatePairFromVals(attribute, attrIndex, value, valIndex));
			return names;
		}

		private static KeyValuePair<string, string> CreatePairFromVals(char[] attribute, int attrIndex, char[] value, int valIndex)
		{
			string attributeString = new string(attribute).Substring(0, attrIndex);
			string valueString = new string(value).Substring(0, valIndex);
			return new KeyValuePair<string, string>(attributeString, valueString);
		}
	}
	 */
}